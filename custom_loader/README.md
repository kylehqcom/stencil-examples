# Custom Loader Stencil Example

You may have the requirement to load different templates based application logic/user settings. Eg load for a different locale. You may also have multiple loaders that populate the template collection to match.

This trivial example simply shows that based on a request query param, a different template can be loaded and displayed to the user.
