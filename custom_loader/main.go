package main

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/kylehqcom/stencil"
	"gitlab.com/kylehqcom/stencil/decorator"
	"gitlab.com/kylehqcom/stencil/executor"
	"gitlab.com/kylehqcom/stencil/matcher"
)

type mapLoader struct {
	t map[string]*template.Template
}

func (m mapLoader) Load(name, text string) error {
	t := template.New(name)
	m.t[name] = template.Must(t.Parse(text))
	return nil
}

func (m mapLoader) RegisterFuncs(template.FuncMap) {
}

func (m mapLoader) Yield() *stencil.Collection {
	c := &stencil.Collection{}
	t := template.New("")
	for n, l := range m.t {
		t.AddParseTree(n, l.Tree)
	}
	c.T = t
	return c
}

func main() {
	// Use this custom map loader to simulate a non fileloader, eg perhaps on each web request
	// you need to load your template from a datastore.
	l := mapLoader{}
	l.t = make(map[string]*template.Template)
	l.Load("a", `A is the content. Try <a href="http://localhost:80?l=b">http://localhost:80?l=b</a> or <a href="http://localhost:80?l=c">http://localhost:80?l=c</a>`)
	l.Load("b", `So much B!!! Try <a href="http://localhost:80?l=a">http://localhost:80?l=a</a> or <a href="http://localhost:80?l=c">http://localhost:80?l=c</a>`)
	l.Load("c", `Curiosity killed the cat... Try <a href="http://localhost:80?l=a">http://localhost:80?l=a</a> or <a href="http://localhost:80?l=b">http://localhost:80?l=b</a>`)

	d := decorator.NewRequestDecorator(
		l.Yield(),
		matcher.NewFilepathMatcher(),
		executor.NewTemplateExecutor(),
	)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Request ?l=a or ?l=b or ?l=c
		vals := r.URL.Query()
		load := vals.Get("l")
		if load != "b" && load != "c" {
			load = "a"
		}
		w.Header().Set("Content-Type", "text/html")
		d.Decorate(w, r, load, nil)
	})
	log.Println("Enjoy using Stencil")
	log.Println("Example now running on http://localhost:80")
	log.Fatal(http.ListenAndServe("", nil))
}
