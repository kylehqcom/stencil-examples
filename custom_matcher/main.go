package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"time"

	"gitlab.com/kylehqcom/stencil"
	"gitlab.com/kylehqcom/stencil/decorator"
	"gitlab.com/kylehqcom/stencil/executor"
	"gitlab.com/kylehqcom/stencil/loader"
)

type maintMatcher struct{}

func (m maintMatcher) Match(c *stencil.Collection, pattern string) (*template.Template, error) {
	t := template.New("")
	return template.Must(t.Parse(fmt.Sprintf("Custom matcher that returns the maintenance template on every match. Pattern: %s", pattern))), nil
}

func main() {
	l := loader.NewFilepathLoader(loader.WithMustParse(true))
	l.RegisterFuncs(template.FuncMap{
		"render": func() string {
			return "Content from render func"
		},
		"flashIsContentTypeHTML": func(fct stencil.FlashContentType) bool {
			return fct == stencil.FlashContentTypeHTML
		},
		"flashIsContentTypeJS": func(fct stencil.FlashContentType) bool {
			return fct == stencil.FlashContentTypeJS
		},
		"flashIsContentTypeString": func(fct stencil.FlashContentType) bool {
			return fct == stencil.FlashContentTypeString
		},
	})
	l.LoadFromFilepath("../_templates/*.html")

	d := decorator.NewRequestDecorator(
		l.Yield(),
		maintMatcher{},
		executor.NewTemplateExecutor(),
	)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		// Using time to prove that any template name will render the maint matcher template.
		d.Decorate(w, r, time.Now().String(), nil)
	})
	log.Println("Enjoy using Stencil")
	log.Println("Example now running on http://localhost:80")
	log.Fatal(http.ListenAndServe("", nil))
}
