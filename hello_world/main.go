package main

import (
	"log"
	"net/http"

	"gitlab.com/kylehqcom/stencil/decorator"
	"gitlab.com/kylehqcom/stencil/executor"
	"gitlab.com/kylehqcom/stencil/loader"
	"gitlab.com/kylehqcom/stencil/matcher"
)

func main() {
	l := loader.NewFilepathLoader(loader.WithMustParse(true))
	l.Load("hello_world", "Hello world and welcome to using Stencil!!!")
	d := decorator.NewRequestDecorator(
		l.Yield(),
		matcher.NewFilepathMatcher(),
		executor.NewTemplateExecutor(),
	)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		d.Decorate(w, r, "hello_world", nil)
	})
	log.Println("Enjoy using Stencil")
	log.Println("Example now running on http://localhost:80")
	log.Fatal(http.ListenAndServe("", nil))
}
