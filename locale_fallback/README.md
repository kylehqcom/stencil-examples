# Locale Fallbacks Stencil Example

Stencil will match templates on names and a locale if passed/setup. On no match, Stencil will "fallback" to the base locale if present, otherwise will fallback to just the name of the template given. Finally returning an error if not found.

This example code follows these fallback rules

```Go
// Will render the EN index.html
http://localhost
http://localhost?locale=en
http://localhost?locale=nosuchlocale


// Will render the ES index.html
http://localhost?locale=es

// Will render the ES error template
http://localhost?locale=es&error=1

// Will render the fallback error EN template
http://localhost?error=1
http://localhost?locale=en&error=1
http://localhost?locale=nosuchlocale&error=1
```
