package main

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/kylehqcom/stencil"
	"gitlab.com/kylehqcom/stencil/decorator"
	"gitlab.com/kylehqcom/stencil/executor"
	"gitlab.com/kylehqcom/stencil/loader"
	"gitlab.com/kylehqcom/stencil/matcher"
)

//
// Example outputs
//
// Will render the EN index.html
// http://localhost
// http://localhost?locale=en
// http://localhost?locale=nosuchlocale
//
// Will render the ES index.html
// http://localhost?locale=es
//
// Will render the ES error template
// http://localhost?locale=es&error=1

// Will render the fallback error EN template
// http://localhost?error=1
// http://localhost?locale=en&error=1
// http://localhost?locale=nosuchlocale&error=1

var l = loader.NewFilepathLoader(loader.WithMustParse(true))

func main() {
	l.RegisterFuncs(template.FuncMap{
		"render": func() string {
			return "Content from render func"
		},
		"flashIsContentTypeHTML": func(fct stencil.FlashContentType) bool {
			return fct == stencil.FlashContentTypeHTML
		},
		"flashIsContentTypeJS": func(fct stencil.FlashContentType) bool {
			return fct == stencil.FlashContentTypeJS
		},
		"flashIsContentTypeString": func(fct stencil.FlashContentType) bool {
			return fct == stencil.FlashContentTypeString
		},
	})
	l.LoadFromFilepath("../_templates/*.html")
	l.LoadFromFilepath("../_templates/*/*.html")

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		render(w, r, "index.html", nil)
	})
	log.Println("Enjoy using Stencil")
	log.Println("Example now running on http://localhost:80")
	log.Fatal(http.ListenAndServe("", nil))
}

func render(w http.ResponseWriter, r *http.Request, name string, data map[string]interface{}, opts ...decorator.Option) {
	vals := r.URL.Query()
	loc := vals.Get("locale")
	if loc != "es" {
		loc = "en"
	}

	if "" != vals.Get("error") {
		name = "not a template that can be matched so will render error"
	}

	d := decorator.NewRequestDecorator(
		l.Yield(),
		matcher.NewFilepathMatcher(
			matcher.WithPathRoot("../_templates"),
			matcher.WithFallbackLocale("en"),
			matcher.WithLocale(loc),
		),
		executor.NewTemplateExecutor(),
		decorator.WithBaseTemplate("layout.html"), decorator.WithErrorTemplate("error.html"),
	)
	d.Decorate(w, r, name, data, opts...)
}
