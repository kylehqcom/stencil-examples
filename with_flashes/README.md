# Flash Message Stencil Example

Stencil provides a handy mechanism to render/display user flash messages. There are x4 pre canned Flash Types, `Alert`, `Error`, `Info` and `Success` but you can create your own flash types as required also.

With flash types defined, you can then check the type of flash message to be rendered and add css or js behaviours as required. One thing to note, as Go escapes any rendered template output, you may need to bind the "WithContentType" option. eg `WithFlashMessageAsHTML`, `WithFlashMessageAsJS` and the default `WithFlashMessageAsString`. This will allow you to render html tags in your flash messages for example.
