package main

import (
	"html/template"
	"log"
	"net/http"

	"gitlab.com/kylehqcom/stencil"
	"gitlab.com/kylehqcom/stencil/decorator"
	"gitlab.com/kylehqcom/stencil/executor"
	"gitlab.com/kylehqcom/stencil/loader"
	"gitlab.com/kylehqcom/stencil/matcher"
)

func main() {
	l := loader.NewFilepathLoader(loader.WithMustParse(true))
	l.RegisterFuncs(template.FuncMap{
		"render": func() string {
			return "Content from render func"
		},
		"flashIsContentTypeHTML": func(fct stencil.FlashContentType) bool {
			return fct == stencil.FlashContentTypeHTML
		},
		"flashIsContentTypeJS": func(fct stencil.FlashContentType) bool {
			return fct == stencil.FlashContentTypeJS
		},
		"flashIsContentTypeString": func(fct stencil.FlashContentType) bool {
			return fct == stencil.FlashContentTypeString
		},
	})
	l.LoadFromFilepath("../_templates/*.html")

	d := decorator.NewRequestDecorator(
		l.Yield(),
		matcher.NewFilepathMatcher(matcher.WithPathRoot("../_templates")),
		executor.NewTemplateExecutor(),
	)

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		r = stencil.AddFlashAlert(r, `Flash alert <a href="#">Example html escaping using the template/html package</a>`, nil,
			stencil.WithFlashMessageAsHTML(),
		)
		r = stencil.AddFlashError(r, "Flash error", nil)
		r = stencil.AddFlashInfo(r, "Flash info", nil)
		r = stencil.AddFlashSuccess(r, "Flash success", nil)
		w.Header().Set("Content-Type", "text/html")
		d.Decorate(w, r, "layout.html", nil)
	})
	log.Println("Enjoy using Stencil")
	log.Println("Example now running on http://localhost:80")
	log.Fatal(http.ListenAndServe("", nil))
}
