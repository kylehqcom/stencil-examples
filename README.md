# stencil-examples

A separate repo to example the https://gitlab.com/kylehqcom/stencil Go Templating Package

### Examples

- [Hello World](hello_world)
- [Custom Loader](custom_loader)
- [Custom Matcher](custom_matcher)
- [Flash Messages](with_flashes)
- [Locale Fallbacks](locale_fallback)

These examples uses the bundled _"decorator"_ to decorate your requests. This (in my opinion) will cover 95% of your use cases. Note that in order to render the index handler example, we assume that you have a folder structure of

```
your/dir/to/templates/
  error.html
  layout.html
  en/
    index.html
  es/
    index.html
```

```go
package main

import (
  "html/template"
  "log"
  "net/http"

  "gitlab.com/kylehqcom/stencil/decorator"
  "gitlab.com/kylehqcom/stencil/executor"
  "gitlab.com/kylehqcom/stencil/loader"
  "gitlab.com/kylehqcom/stencil/matcher"
)

var l = loader.NewFilepathLoader(loader.WithMustParse(true))

func main() {
  // This example has a yet to be loaded template with a template.func of {{render}}, so we placeholder to ensure parsing
  l.RegisterFuncs(template.FuncMap{"render": func() string {
    return "Render called unexpectedly"
  }})
  l.LoadFromFilepath("your/dir/to/templates/*.html")
  l.LoadFromFilepath("your/dir/to/templates/*/*.html")

  indexHandler := func(w http.ResponseWriter, r *http.Request) {
    render(w, r, "index.html", nil) // Note we only refer to the filename to lookup as the PathRoot option is defined in setup
  }

  http.HandleFunc("/", indexHandler)
  log.Println("Enjoy using Stencil")
  log.Println("Example now running on Port :8080")
  log.Fatal(http.ListenAndServe(":8080", nil))
}

// The placeholded render method from above will get replaced with this render method on template execution
func render(w http.ResponseWriter, r *http.Request, name string, data map[string]interface{}, opts ...decorator.Option) {
  // Pass a query param eg `?locale=es` to render the es template
  vals := r.URL.Query()
  loc := vals.Get("locale")
  if loc != "es" {
    loc = "en"
  }

  d := decorator.NewRequestDecorator(
    l.Yield(),
    matcher.NewFilepathMatcher(
      matcher.WithPathRoot("your/dir/to/templates"),
      matcher.WithFallbackLocale("en"),
      matcher.WithLocale(loc),
    ),
    executor.NewTemplateExecutor(),
    decorator.WithBaseTemplate("layout.html"), decorator.WithErrorTemplate("error.html"),
  )

  // Note that we created the decorator "withOptions" but this render method receivers "opts" will take precedence on the Decorate() call.
  // That way you can have default options defined, but call with different options on render as you require.
  d.Decorate(w, r, name, data, opts...)
}
```
